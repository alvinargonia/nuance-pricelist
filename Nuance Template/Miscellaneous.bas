Attribute VB_Name = "Miscellaneous"

Sub switchToSKU()

    If ActiveSheet.name = "Main" Then
        Sheets("SKU").Visible = True
        Sheets("SKU").Activate
        Sheets("Main").Visible = xlVeryHidden
    Else
        Sheets("Main").Visible = True
        Sheets("Main").Activate
        Sheets("SKU").Visible = xlVeryHidden
    End If

End Sub

Sub resetTemplate()
    Dim wS As Worksheet
    
    For Each wS In ThisWorkbook.Worksheets
        If wS.name <> "Main" And wS.name <> "SKU" And _
          wS.name <> "OrderReq" And wS.name <> "DoNotDelete" _
          And wS.name <> "Directories" Then
            Application.DisplayAlerts = False
            wS.Delete
            Application.DisplayAlerts = True
        End If
    Next wS

End Sub

Sub switchToDirectories()

    If ActiveSheet.name = "Main" Then
        Sheets("Directories").Visible = True
        Sheets("Directories").Activate
        Sheets("Main").Visible = xlVeryHidden
    Else
        Sheets("Main").Visible = True
        Sheets("Main").Activate
        Sheets("Directories").Visible = xlVeryHidden
    End If

End Sub
