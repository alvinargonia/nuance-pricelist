Attribute VB_Name = "SaveModule"

Sub saveRoutine()

    Set templateWB = ThisWorkbook
    Set mainWS = ThisWorkbook.Sheets("Main")

    Application.ScreenUpdating = False
    checkDataToSave
    checkPath
    saveToFolders
    savePriceLevelVersions
    
    resetTemplate
    Application.ScreenUpdating = True
    
    MsgBox "Price Lists have been saved.", vbExclamation, "Done!"
    
End Sub

Sub checkDataToSave()
    Dim wS As Worksheet
    Dim thisCounter As Integer
    
    thisCounter = 0
    For Each wS In templateWB.Worksheets
        DoEvents
        If wS.name = "Imaging" Or wS.name = "Dictation" Then
            thisCounter = thisCounter + 1
        End If
    Next wS
    
    If thisCounter = 0 Then
        MsgBox "No Price List to save." & vbLf & vbLf & "Run the template first.", vbExclamation, "Alert!"
        End
    End If

End Sub
Sub checkPath()
    Dim path As Variant
    
    filePath(1) = Trim(templateWB.Sheets("Directories").Cells(7, 4).Text)
    filePath(2) = Trim(templateWB.Sheets("Directories").Cells(8, 4).Text)
    filePath(3) = Trim(templateWB.Sheets("Directories").Cells(9, 4).Text)
    
    For Each path In filePath
        DoEvents
        If Right(path, 1) <> "\" Then
            path = path & "\"
        End If
        If Dir(path, vbDirectory) = "" Then
            MsgBox "No access to" & vbLf & vbLf & path & vbLf & vbLf & _
              "Please check and try again.", vbExclamation, "Alert!"
            End
        End If
    Next path

End Sub
Sub saveToFolders()
    Dim wS As Worksheet
    Dim savePath As String
    Dim pricelistDate As String
    Dim dataDate As Variant
    
    copyToNewWorkbook
    
    dataDate = Split(newBook.Sheets(1).Cells(5, 1).Text, " ")
    fileName = dataDate(2) & " " & dataDate(0) & " " & dataDate(1)
    savePath = filePath(1) & dataDate(0) & "\"
    Call saveFile(savePath, fileName)

    newBook.Sheets("Imaging").Range(Sheets("Imaging").Cells(14, 10), Sheets("Imaging").Cells(LastRow(, Sheets("Imaging"), newBook), LastColumn(, Sheets("Imaging"), newBook))).Copy
    newBook.Sheets("Imaging").Cells(14, 10).PasteSpecial xlPasteValues
    newBook.Sheets("Imaging").Activate
    newBook.Sheets("Imaging").Cells(1, 1).Select
    newBook.Sheets("Dictation").Range(Sheets("Dictation").Cells(14, 9), Sheets("Dictation").Cells(LastRow(, Sheets("Dictation"), newBook), LastColumn(, Sheets("Dictation"), newBook))).Copy
    newBook.Sheets("Dictation").Cells(14, 9).PasteSpecial xlPasteValues
    newBook.Sheets("Dictation").Range(Sheets("Dictation").Cells(14, 9), Sheets("Dictation").Cells(LastRow(, Sheets("Dictation"), newBook), LastColumn(, Sheets("Dictation"), newBook))).NumberFormat = "0.00"
    newBook.Sheets("Dictation").Activate
    newBook.Sheets("Dictation").Cells(1, 1).Select
    Application.CutCopyMode = False
    
    savePath2 = filePath(2) & dataDate(0) & "\"
    Call saveFile(savePath2, fileName)
    savePath3 = filePath(3) & dataDate(0) & "\"
    
    Application.DisplayAlerts = False
    newBook.Close
    Application.DisplayAlerts = True

End Sub
Sub copyToNewWorkbook()

    Set newBook = Workbooks.Add
    
    For Each wS In templateWB.Worksheets
        DoEvents
        If wS.name <> "Main" And wS.name <> "DoNotDelete" And wS.name <> "SKU" And wS.name <> "OrderReq" And wS.name <> "Directories" Then
            templateWB.Sheets(wS.name).Copy after:=newBook.Sheets(Sheets.Count)
        End If
    Next wS
    
    Application.DisplayAlerts = False
    newBook.Sheets(1).Delete
    Application.DisplayAlerts = True
    newBook.Sheets(1).Activate

End Sub

Sub saveFile(savePath As String, fileName As String)
    Dim fullPath As String
    
    fullPath = savePath & fileName & ".xlsx"

    If Dir(savePath, vbDirectory) = "" Then
        MkDir (savePath)
    End If
    
    If Dir(fullPath) <> "" Then
        If MsgBox(fileName & " already exists in: " & vbLf & vbLf & savePath & vbLf & vbLf & "Overwrite with the new file?", vbYesNo + vbDefaultButton2 + vbQuestion, "Overwrite?") = vbYes Then
            Application.DisplayAlerts = False
            ActiveWorkbook.SaveAs (fullPath)
            Application.DisplayAlerts = True
        Else
            Exit Sub
        End If
    Else
        newBook.SaveAs (fullPath)
    End If

End Sub

Sub savePriceLevelVersions()
    Dim fileNameArray(1 To 2) As Variant
    Dim name As Variant
    Dim rowCounter As Integer
    Dim wS As Worksheet
    Dim channelValue As String
    
    fileNameArray(1) = " VAR"
    fileNameArray(2) = " Non-VAR"
    
    For Each name In fileNameArray
        rowCounter = 13
        DoEvents
        Do Until IsEmpty(mainWS.Cells(rowCounter, 9))
            DoEvents
            copyToNewWorkbook
            For Each wS In newBook.Worksheets
                DoEvents
                If wS.name = "Imaging" Or wS.name = "Dictation" Then
                    DoEvents

                    wS.Range("I:" & columnNumberToLetter(LastColumn(13, wS, newBook))).Copy
                    wS.Range("I:" & columnNumberToLetter(LastColumn(13, wS, newBook))).PasteSpecial xlPasteValues
                    wS.Range("G:H").Delete
                    
                    If name = " VAR" Then
                        channelValue = "NON VAR"
                    Else
                        channelValue = "VAR"
                    End If
                    wS.Rows(13).AutoFilter field:=5, Criteria1:=channelValue
                    If wS.UsedRange.Rows.SpecialCells(xlCellTypeVisible).Rows.Count > 13 Then
                        Application.DisplayAlerts = False
                        wS.UsedRange.Offset(13, 0).Resize(wS.UsedRange.Rows.Count - 13).Rows.SpecialCells(xlCellTypeVisible).Delete
                        wS.AutoFilterMode = False
                        Application.DisplayAlerts = True
                    Else
                        wS.AutoFilterMode = False
                    End If

                    colCounter = 9
                    Do Until IsEmpty(wS.Cells(13, colCounter))
                        DoEvents
                        If wS.Cells(13, colCounter).Text <> mainWS.Cells(rowCounter, 9).Text And wS.Cells(13, colCounter).Text <> "RRP" Then
                            wS.Columns(colCounter).Delete
                        Else
                            colCounter = colCounter + 1
                        End If
                    Loop
                    
                    wS.Range(wS.Cells(9, LastColumn(13, wS, newBook)), wS.Cells(11, LastColumn(13, wS, newBook))).Borders(xlEdgeRight).LineStyle = xlContinuous
                    wS.Activate
                    wS.Cells(13, 1).Select
                End If
            Next wS

            If Dir(savePath3, vbDirectory) = "" Then
                MkDir (savePath3)
            End If
            
            Call saveFile(savePath3 & mainWS.Cells(rowCounter, 9).Text & "\", fileName & name)
            Application.DisplayAlerts = False
            newBook.Close
            Application.DisplayAlerts = True
            
            rowCounter = rowCounter + 1
        Loop
    Next name

End Sub
