Attribute VB_Name = "zz_AuxModule"
Option Explicit
Option Compare Text

Const masterPassword = "223344"

Public Function URLEncode(ByVal StringToEncode As String, Optional UsePlusRatherThanHexForSpace As Boolean = False) As String

  Dim TempAns As String
  Dim CurChr As Integer
  CurChr = 1

  Do Until CurChr - 1 = Len(StringToEncode)
    Select Case Asc(Mid(StringToEncode, CurChr, 1))
      Case 48 To 57, 65 To 90, 97 To 122
        TempAns = TempAns & Mid(StringToEncode, CurChr, 1)
      Case 32
        If UsePlusRatherThanHexForSpace = True Then
          TempAns = TempAns & "+"
        Else
          TempAns = TempAns & "%" & Hex(32)
        End If
      Case Else
        TempAns = TempAns & "%" & _
          Right("0" & Hex(Asc(Mid(StringToEncode, CurChr, 1))), 2)
    End Select

    CurChr = CurChr + 1
  Loop

  URLEncode = TempAns
End Function

Sub protectSheet(Optional sheetName As String, Optional workbookName As Workbook)

If sheetName <> vbNullString Then
    If workbookName Is Nothing Then
        Sheets(sheetName).Protect masterPassword, DrawingObjects:=True, Contents:=True, Scenarios:=True, AllowSorting:=True, AllowFiltering:=True, AllowDeletingRows:=True, AllowInsertingRows:=True
    Else
        workbookName.Sheets(sheetName).Protect masterPassword, DrawingObjects:=True, Contents:=True, Scenarios:=True, AllowSorting:=True, AllowFiltering:=True, AllowDeletingRows:=True, AllowInsertingRows:=True
    End If
Else
    ActiveSheet.Protect masterPassword, DrawingObjects:=True, Contents:=True, Scenarios:=True, AllowSorting:=True, AllowFiltering:=True, AllowDeletingRows:=True, AllowInsertingRows:=True
End If

End Sub

Sub unProtectSheet(Optional sheetName As String, Optional workbookName As Workbook)

If sheetName <> vbNullString Then
    If workbookName Is Nothing Then
        Sheets(sheetName).Unprotect masterPassword
    Else
        workbookName.Sheets(sheetName).Unprotect masterPassword
    End If
Else
    ActiveSheet.Unprotect masterPassword
End If

End Sub

'Sub deLayer(ByVal objIEone As InternetExplorer)
'
'Do While objIEone.Busy
'    DoEvents
'Loop
'
'Do
'    DoEvents
'Loop Until objIEone.ReadyState = READYSTATE_COMPLETE
'
'End Sub

Sub updateStatusBar(ByVal textMessage As String, Optional ByVal startTime As Date, Optional ByVal sheetName As String, Optional ByVal X As Long)

Dim elapsedTime As Date
Dim elapsedTimeStr As String

If sheetName <> "" Then
    elapsedTime = DateDiff("s", startTime, Now)
    elapsedTimeStr = [elapsedTime] \ 60 & Format([elapsedTime] Mod 60, "\:00")
    Application.StatusBar = textMessage & "   |   " & Format(X / LastRow(, Sheets(sheetName)), "0.00%") & "   |   Elapsed Time: " & elapsedTimeStr
Else
    Application.StatusBar = textMessage
End If

End Sub

Public Function columnNumberToLetter(ByVal lngNumber As Long) As String
    columnNumberToLetter = Split(ThisWorkbook.Worksheets(1).Columns(lngNumber).Address, ":")(0)
    
    columnNumberToLetter = Replace(columnNumberToLetter, "$", "")
End Function

Public Function columnLetterToNumber(ByVal strLetter As String) As Long
    columnLetterToNumber = ThisWorkbook.Worksheets(1).Columns(strLetter).Column
End Function

Function LastRow(Optional ByVal columnLetter As String, Optional ByVal targetSheet As Worksheet, Optional ByVal targetBook As Workbook) As Long

If targetBook Is Nothing Then Set targetBook = ActiveWorkbook
If targetSheet Is Nothing Then Set targetSheet = ActiveSheet

If columnLetter = vbNullString Then
    LastRow = targetBook.Sheets(targetSheet.name).UsedRange.row - 1 + targetBook.Sheets(targetSheet.name).UsedRange.Rows.Count
Else
    LastRow = targetBook.Sheets(targetSheet.name).Cells(targetBook.Sheets(targetSheet.name).Rows.Count, columnLetter).End(xlUp).row
End If

End Function

Function LastColumn(Optional ByVal rowNumber As Long, Optional ByVal targetSheet As Worksheet, Optional ByVal targetBook As Workbook) As Long

If targetBook Is Nothing Then Set targetBook = ActiveWorkbook
If targetSheet Is Nothing Then Set targetSheet = ActiveSheet

If rowNumber = 0 Then
    LastColumn = targetBook.Sheets(targetSheet.name).UsedRange.Column - 1 + targetBook.Sheets(targetSheet.name).UsedRange.Columns.Count
Else
    LastColumn = targetBook.Sheets(targetSheet.name).Cells(rowNumber, targetBook.Sheets(targetSheet.name).Columns.Count).End(xlToLeft).Column
End If

End Function

Function FindAll(SearchRange As Range, _
                FindWhat As Variant, _
               Optional LookIn As XlFindLookIn = xlValues, _
                Optional LookAt As XlLookAt = xlWhole, _
                Optional SearchOrder As XlSearchOrder = xlByRows, _
                Optional MatchCase As Boolean = False, _
                Optional BeginsWith As String = vbNullString, _
                Optional EndsWith As String = vbNullString, _
                Optional BeginEndCompare As VbCompareMethod = vbTextCompare) As Range
Dim foundCell As Range
Dim FirstFound As Range
Dim LastCell As Range
Dim ResultRange As Range
Dim XLookAt As XlLookAt
Dim Include As Boolean
Dim CompMode As VbCompareMethod
Dim Area As Range
Dim MaxRow As Long
Dim MaxCol As Long


CompMode = BeginEndCompare
If BeginsWith <> vbNullString Or EndsWith <> vbNullString Then
    XLookAt = xlPart
Else
    XLookAt = LookAt
End If

For Each Area In SearchRange.Areas
    With Area
        If .Cells(.Cells.CountLarge).row > MaxRow Then
            MaxRow = .Cells(.Cells.CountLarge).row
        End If
        If .Cells(.Cells.CountLarge).Column > MaxCol Then
            MaxCol = .Cells(.Cells.CountLarge).Column
        End If
    End With
Next Area
Set LastCell = SearchRange.Worksheet.Cells(MaxRow, MaxCol)


On Error GoTo 0
Set foundCell = SearchRange.Find(What:=FindWhat, _
        after:=LastCell, _
        LookIn:=LookIn, _
        LookAt:=xlPart, _
        SearchOrder:=SearchOrder, _
        MatchCase:=MatchCase)

If Not foundCell Is Nothing Then
    Set FirstFound = foundCell
    Set ResultRange = foundCell
    Set foundCell = SearchRange.FindNext(after:=foundCell)
    Do Until False ' Loop forever. We'll "Exit Do" when necessary.
        If (foundCell Is Nothing) Then
            Exit Do
        End If
        If (foundCell.Address = FirstFound.Address) Then
            Exit Do
        End If
        Include = False
            
        If BeginsWith = vbNullString Then
            If EndsWith = vbNullString Then
                Include = True
            Else
                If Len(foundCell.Text) < Len(EndsWith) Then
                    Include = False
                Else
                    If StrComp(Right(foundCell.Text, Len(EndsWith)), EndsWith, CompMode) = 0 Then
                        Include = True
                    Else
                        Include = False
                    End If
                End If
            End If
        End If
        If EndsWith = vbNullString Then
            If BeginsWith = vbNullString Then
                Include = True
            Else
                If StrComp(Left(foundCell.Text, Len(BeginsWith)), BeginsWith, CompMode) = 0 Then
                    Include = True
                Else
                    Include = False
                End If
            End If
        Else
            If Len(foundCell.Text) < Len(EndsWith) Then
                Include = False
            Else
                If StrComp(Right(foundCell.Text, Len(EndsWith)), EndsWith, CompMode) = 0 Then
                    Include = True
                Else
                    Include = False
                End If
            End If
        End If
        
        If Include = True Then
            Set ResultRange = Application.Union(ResultRange, foundCell)
        End If
        Set foundCell = SearchRange.FindNext(after:=foundCell)
    Loop
End If
    
Set FindAll = ResultRange

End Function

Function FindAllOnWorksheets(InWorkbook As Workbook, _
                InWorksheets As Variant, _
                SearchAddress As String, _
                FindWhat As Variant, _
                Optional LookIn As XlFindLookIn = xlValues, _
                Optional LookAt As XlLookAt = xlWhole, _
                Optional SearchOrder As XlSearchOrder = xlByRows, _
                Optional MatchCase As Boolean = False, _
                Optional BeginsWith As String = vbNullString, _
                Optional EndsWith As String = vbNullString, _
                Optional BeginEndCompare As VbCompareMethod = vbTextCompare) As Variant
Dim WSArray() As String
Dim wS As Worksheet
Dim WB As Workbook
Dim ResultRange() As Range
Dim WSNdx As Long
Dim R As Range
Dim SearchRange As Range
Dim FoundRange As Range
Dim WSS As Variant
Dim n As Long


If InWorkbook Is Nothing Then
    Set WB = ActiveWorkbook
Else
    Set WB = InWorkbook
End If

If IsEmpty(InWorksheets) = True Then
    With WB.Worksheets
        ReDim WSArray(1 To .Count)
        For WSNdx = 1 To .Count
            WSArray(WSNdx) = .Item(WSNdx).name
        Next WSNdx
    End With

Else
    If IsObject(InWorksheets) = True Then
        If TypeOf InWorksheets Is Excel.Worksheet Then
            If StrComp(InWorksheets.Parent.name, WB.name, vbTextCompare) <> 0 Then
                Exit Function
            Else
                ReDim WSArray(1 To 1)
                WSArray(1) = InWorksheets.name
            End If
        Else
        End If
    Else
        If IsArray(InWorksheets) = True Then
            ReDim WSArray(LBound(InWorksheets) To UBound(InWorksheets))
            For WSNdx = LBound(InWorksheets) To UBound(InWorksheets)
                If IsObject(InWorksheets(WSNdx)) = True Then
                    If TypeOf InWorksheets(WSNdx) Is Excel.Worksheet Then
                        WSArray(WSNdx) = InWorksheets(WSNdx).name
                    Else
                        Exit Function
                    End If
                Else
                    Select Case UCase(TypeName(InWorksheets(WSNdx)))
                        Case "LONG", "INTEGER"
                            Err.Clear
                            Set wS = WB.Worksheets(InWorksheets(WSNdx))
                            If Err.Number <> 0 Then
                                Exit Function
                            End If
                            WSArray(WSNdx) = WB.Worksheets(InWorksheets(WSNdx)).name
                        Case "STRING"
                            Err.Clear
                            Set wS = WB.Worksheets(InWorksheets(WSNdx))
                            If Err.Number <> 0 Then
                                Exit Function
                            End If
                            WSArray(WSNdx) = InWorksheets(WSNdx)
                    End Select
                End If
            Next WSNdx
        Else
            Select Case UCase(TypeName(InWorksheets))
                Case "INTEGER", "LONG"
                    Err.Clear
                    Set wS = WB.Worksheets(InWorksheets)
                    If Err.Number <> 0 Then
                        Exit Function
                    Else
                        WSArray = Array(WB.Worksheets(InWorksheets).name)
                    End If
                Case "STRING"
                    If InStr(1, InWorksheets, ":", vbBinaryCompare) > 0 Then
                        WSS = Split(InWorksheets, ":")
                        Err.Clear
                        n = LBound(WSS)
                        If Err.Number <> 0 Then
                            Exit Function
                        End If
                        If LBound(WSS) > UBound(WSS) Then
                            Exit Function
                        End If
                            
                                                
                        ReDim WSArray(LBound(WSS) To UBound(WSS))
                        For n = LBound(WSS) To UBound(WSS)
                            Err.Clear
                            Set wS = WB.Worksheets(WSS(n))
                            If Err.Number <> 0 Then
                                Exit Function
                            End If
                            WSArray(n) = WSS(n)
                         Next n
                    Else
                        Err.Clear
                        Set wS = WB.Worksheets(InWorksheets)
                        If Err.Number <> 0 Then
                            Exit Function
                        Else
                            WSArray = Array(InWorksheets)
                        End If
                    End If
            End Select
        End If
    End If
End If
On Error Resume Next
For WSNdx = LBound(WSArray) To UBound(WSArray)
    Err.Clear
    Set wS = WB.Worksheets(WSArray(WSNdx))
    If Err.Number <> 0 Then
        Exit Function
    End If
    Err.Clear
    Set R = WB.Worksheets(WSArray(WSNdx)).Range(SearchAddress)
    If Err.Number <> 0 Then
        Exit Function
    End If
Next WSNdx

ReDim ResultRange(LBound(WSArray) To UBound(WSArray))
For WSNdx = LBound(WSArray) To UBound(WSArray)
    Set wS = WB.Worksheets(WSArray(WSNdx))
    Set SearchRange = wS.Range(SearchAddress)
    Set FoundRange = FindAll(SearchRange:=SearchRange, _
                    FindWhat:=FindWhat, _
                    LookIn:=LookIn, LookAt:=LookAt, _
                    SearchOrder:=SearchOrder, _
                    MatchCase:=MatchCase, _
                    BeginsWith:=BeginsWith, _
                    EndsWith:=EndsWith)
    
    If FoundRange Is Nothing Then
        Set ResultRange(WSNdx) = Nothing
    Else
        Set ResultRange(WSNdx) = FoundRange
    End If
Next WSNdx

FindAllOnWorksheets = ResultRange

End Function

Public Function FileFolderExists(strFullPath As String) As Boolean
    On Error GoTo EarlyExit
    If Not Dir(strFullPath, vbDirectory) = vbNullString Then FileFolderExists = True
    
EarlyExit:
    On Error GoTo 0
End Function

Function FileLastModified(strFullFileName As String)
    Dim fs As Object, f As Object, s As String
     
    Set fs = CreateObject("Scripting.FileSystemObject")
    Set f = fs.GetFile(strFullFileName)
     
    s = f.DateLastModified
    s = Trim(Left(s, InStr(s, " ")))
    FileLastModified = s
     
    Set fs = Nothing
    Set f = Nothing
     
End Function

Function isalpha(str As String) As Boolean
 
Dim i As Integer
Dim Flag As Boolean
Flag = True
 
For i = 1 To Len(Trim(str))
 
    If Asc(Mid(Trim(str), i, 1)) > 64 And Asc(Mid(Trim(str), i, 1)) < 90 Or _
    Asc(Mid(Trim(str), i, 1)) > 96 And Asc(Mid(Trim(str), i, 1)) < 123 Then
 
    Else
        Flag = False
    End If
Next i
 
If Flag = True Then
    isalpha = True
Else
    isalpha = False
End If
 
End Function


