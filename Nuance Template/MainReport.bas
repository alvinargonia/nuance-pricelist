Attribute VB_Name = "MainReport"
Option Explicit
Public templateWB As Workbook
Public data1WB As Workbook
Public data2WB As Workbook
Public mainWS As Worksheet
Public tempWS As Worksheet
Public dataDate As String
Public newBook As Workbook
Public fileName As String
Public filePath(1 To 3) As Variant
Public savePath2 As String
Public savePath3 As String

Sub mainRoutine()

    Set templateWB = ThisWorkbook
    Application.DisplayAlerts = False
    Application.ScreenUpdating = False
    
    checkOpenFiles
    prepTemplate
    copyDataFromPricelist
    completeColumns
    
    Application.DisplayAlerts = True
    Application.ScreenUpdating = True
    
    templateWB.Sheets("Main").Activate
    MsgBox "Done running the Price List template." & vbLf & vbLf & "Please review the reports before saving.", vbInformation, "Done!"
    
End Sub

Sub checkOpenFiles()
    Dim WB As Workbook
    Dim data1Count As Integer
    Dim data2Count As Integer
    
    data1Count = 0
    data2Count = 0
    For Each WB In Application.Workbooks
        DoEvents
        If InStr(1, LCase(WB.name), "nuance desktop pricelist core imaging master") <> 0 Then
            Set data1WB = Application.Workbooks(WB.name)
            data1Count = data1Count + 1
        End If
        If InStr(1, LCase(WB.name), "nuance dictation ingram micro") <> 0 Then
            Set data2WB = Application.Workbooks(WB.name)
            data2Count = data2Count + 1
        End If
    Next WB
    
    If data1WB Is Nothing Then
        If data2WB Is Nothing Then
            MsgBox "Nuance Imaging and Dictation Price Lists are not open." & vbLf & vbLf & _
              "Please open and try again.", vbExclamation, "Alert!"
            End
        Else
            MsgBox "Nuance Imaging Price List is not open." & vbLf & vbLf & _
              "Please open and try again.", vbExclamation, "Alert!"
            End
        End If
    Else
        If data2WB Is Nothing Then
            MsgBox "Nuance Dictation Price List is not open." & vbLf & vbLf & _
              "Please open and try again.", vbExclamation, "Alert!"
            End
        End If
    End If
    
    If data1Count > 1 Then
        MsgBox "Multiple instances of Nuance Imaging Price Lists are open." & vbLf & vbLf & "Please close all but one and try again.", vbExclamation, "Alert!"
        End
    ElseIf data2Count > 1 Then
        MsgBox "Multiple instances of Nuance Dictation Price Lists are open." & vbLf & vbLf & "Please close all but one and try again.", vbExclamation, "Alert!"
        End
    End If

End Sub

Sub prepTemplate()
    Dim wS As Worksheet
    
    If templateWB.Sheets("Main").Cells(11, 8).Text = "" Then
        templateWB.Sheets("Main").Cells(11, 8).Select
        MsgBox "Please indicate the MONTH and YEAR of the price list to be generated.", vbExclamation, "Attention!"
        End
    End If
    
    For Each wS In templateWB.Worksheets
        DoEvents
        If wS.name = "Imaging" Or wS.name = "Dictation" Or wS.name = "Order Requirements" Then
            wS.Delete
        End If
    Next wS
    
    templateWB.Sheets("DoNotDelete").Visible = True
    templateWB.Sheets("DoNotDelete").Copy after:=templateWB.Sheets(Sheets.Count)
    ActiveSheet.name = "Imaging"
    addColumns ("Imaging")
    templateWB.Sheets("DoNotDelete").Copy after:=templateWB.Sheets(Sheets.Count)
    ActiveSheet.name = "Dictation"
    addColumns ("Dictation")
    templateWB.Sheets("DoNotDelete").Visible = xlVeryHidden
    templateWB.Sheets("OrderReq").Visible = True
    templateWB.Sheets("OrderReq").Copy after:=templateWB.Sheets(Sheets.Count)
    ActiveSheet.name = "Order Requirements"
    templateWB.Sheets("OrderReq").Visible = xlVeryHidden

End Sub

Sub addColumns(sheetName As String)
    Dim colLast As Integer
    
    If IsEmpty(templateWB.Sheets("Main").Range("I13")) Then
        Exit Sub
    End If
    
    templateWB.Sheets("Main").Range("I13", Sheets("Main").Range("I13").End(xlDown)).Copy
    templateWB.Sheets(sheetName).Cells(13, 10).PasteSpecial Paste:=xlPasteValues, Transpose:=True

    With templateWB.Sheets(sheetName).Cells(13, 10)
        .Interior.Color = 16764057
        .Borders.LineStyle = xlContinuous
        .Font.Bold = True
        .NumberFormat = "0.00%"
    End With
    
    templateWB.Sheets(sheetName).Cells(13, 10).Copy
    colLast = LastColumn(13, templateWB.Sheets(sheetName), templateWB)
    templateWB.Sheets(sheetName).Range(Sheets(sheetName).Cells(13, 10), Sheets(sheetName).Cells(13, colLast)).PasteSpecial xlPasteFormats
    Application.CutCopyMode = False

End Sub

Sub copyDataFromPricelist()
    Dim dataArray() As Variant
    Dim dataWSname() As Variant
    Dim dataName() As Variant
    Dim findDate As Variant
    Dim i As Integer
    
    dataArray = Array(data1WB, data2WB)
    dataWSname = Array("Imaging", "Dictation")
    dataName = Array("All in One �", "GBP")
    
    For i = 0 To 1
        DoEvents
        dataArray(i).Sheets(dataName(i)).Copy after:=templateWB.Sheets(Sheets.Count)
        ActiveSheet.name = "Data"
        dataDate = Format(templateWB.Sheets("Main").Cells(11, 8).Text, "mmmm yyyy")
        
        cleanUpData (i)
        
        With templateWB.Sheets("Data")
            .UsedRange.Offset(1, 0).Resize(.UsedRange.Rows.Count - 1).Copy
            templateWB.Sheets(dataWSname(i)).Cells(14, 2).PasteSpecial xlPasteValues
            .Delete
        End With
        
        templateWB.Sheets(dataWSname(i)).Cells(5, 1).Value = dataDate & " Nuance Licensing Product Price List from Ingram Micro"
        templateWB.Sheets(dataWSname(i)).Activate
        templateWB.Sheets(dataWSname(i)).Cells(14, 1).Select
    Next i

End Sub

Sub cleanUpData(i As Integer)
    Dim typeFilter1(1 To 2) As Variant
    Dim typefilter2(1 To 3) As Variant

    typeFilter1(1) = "Box"
    typeFilter1(2) = "Download"
    
    typefilter2(1) = "Box"
    typefilter2(2) = "Download"
    typefilter2(3) = "Hardware"
    
    With templateWB.Sheets("Data")
        .AutoFilterMode = False
        .Rows("1:10").Delete
        If i = 0 Then
            .Rows(1).AutoFilter field:=1, Criteria1:=typeFilter1, Operator:=xlFilterValues
        Else
            .Rows(1).AutoFilter field:=1, Criteria1:=typefilter2, Operator:=xlFilterValues
        End If
        .UsedRange.Offset(1, 0).Resize(.UsedRange.Rows.Count - 1).Rows.SpecialCells(xlCellTypeVisible).Delete
        .AutoFilterMode = False
        .Range("A:B, D:D, I:I, L:L, N:P").Delete
        .Range("D:D").Cut
        .Range("B:B").Insert shift:=xlRight
        .Range("E:E").Cut
        .Range("D:D").Insert shift:=xlRight
    End With

End Sub

Sub completeColumns()
    Dim dataWSname() As Variant
    Dim sheetName As Variant
    Dim thisWS As Worksheet
    Dim rowLast As Integer
    Dim colLast As Integer
    
    dataWSname = Array("Imaging", "Dictation")
    
    For Each sheetName In dataWSname
        Set thisWS = templateWB.Sheets(sheetName)
        
        thisWS.Cells(14, 1).Formula = "=IFERROR(VLOOKUP(F14,SKU!I:J, 2,FALSE), ""Not Found"")"
        thisWS.Cells(14, 1).AutoFill Destination:=Range(thisWS.Cells(14, 1), thisWS.Cells(LastRow(, thisWS, templateWB), 1))
        thisWS.Range(thisWS.Cells(14, 1), thisWS.Cells(LastRow(, thisWS, templateWB), 1)).Copy
        thisWS.Cells(14, 1).PasteSpecial xlPasteValues
        thisWS.Range(thisWS.Cells(14, 7), thisWS.Cells(LastRow(, thisWS, templateWB), 8)).Interior.Color = 13434879
        
        If Not IsEmpty(thisWS.Cells(13, 10)) Then
            With thisWS.Cells(14, 10)
                .Formula = "=IFERROR(ROUND($G14/(1-J$13),2), ""-"")"
                .NumberFormat = "0.00"
                .Font.Bold = True
                .Font.Color = 16711680
                .HorizontalAlignment = xlRight
            End With

            If LastColumn(13, thisWS, templateWB) > 10 Then
                thisWS.Cells(14, 10).AutoFill Destination:=Range(thisWS.Cells(14, 10), thisWS.Cells(14, LastColumn(13, thisWS, templateWB)))
            End If
            thisWS.Range(thisWS.Cells(14, 10), thisWS.Cells(14, LastColumn(13, thisWS, templateWB))).AutoFill _
              Destination:=Range(thisWS.Cells(14, 10), thisWS.Cells(LastRow(, thisWS, templateWB), LastColumn(13, thisWS, templateWB)))
        End If
        With thisWS.Range(thisWS.Cells(9, 1), thisWS.Cells(11, LastColumn(13, thisWS, templateWB)))
            .Interior.Color = 13434828
            .Borders(xlDiagonalDown).LineStyle = xlNone
            .Borders(xlDiagonalUp).LineStyle = xlNone
            .Borders(xlInsideVertical).LineStyle = xlNone
            .Borders(xlInsideHorizontal).LineStyle = xlNone
            .Borders(xlEdgeTop).LineStyle = xlContinuous
            .Borders(xlEdgeRight).LineStyle = xlContinuous
            .Borders(xlEdgeBottom).LineStyle = xlContinuous
        End With
    
        thisWS.Activate
        thisWS.Cells(14, 1).Select
    Next sheetName
    
    templateWB.Sheets("Dictation").Range("I:I").Delete

End Sub
